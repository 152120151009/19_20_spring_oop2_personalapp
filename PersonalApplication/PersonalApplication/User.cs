﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalApplication
{
    public class User
    {
        public String username;
        public String password;
        public bool rememberMe;
        public String usertypes;
     

        
        public User(String username,String password,bool rememberMe,String usertypes)
        {
            this.Username = username;
            this.Password = password;
            this.RememberMe = rememberMe;
            this.UserTypes = usertypes;
        }

        public string Username { get { return username; } set { username = value; } }
        public string Password { get { return password; } set { password = value; } }
        public bool RememberMe { get { return rememberMe; } set { rememberMe = value; } }
        public string UserTypes { get {return usertypes; } set { usertypes = value; } }

        public bool IsValid(string username,string password)
        {
            return this.Username.Equals(username) && this.password.Equals(Util.ComputeSha256Hash(password));
        }
        public string Tostring()
        {
            return Username + "," + Password + "," + (rememberMe ? "1" : "0")+","+usertypes ;
        }
    }
}
