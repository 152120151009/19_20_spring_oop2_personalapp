﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonalApplication
{
    public partial class LoginForm : Form
    {
        public static List<User> userList = new List<User>();
        public  const string userFilePath = @"data/user.csv";
        public LoginForm()
        {
            InitializeComponent();
           
            checkRememberedUser();
            /* userList.Add(new User("Nail", "6208ef0f7750c111548cf90b6ea1d0d0a66f6bff40dbef07cb45ec436263c7d6"));//58
              userList.Add(new User("Kagan", "c21c1a4d4f1e71a2f371d4431b92639129dedb0d4674c6c9ef97605bd321040c"));//5858
              userList.Add(new User("Alperen", "5f9c4ab08cac7457e9111a30e4664920607ea2c115a1433d7be98e97e64244ca"));//26*/

        }

        public void btnLogin_Click(object sender, EventArgs e)
        {
            Util.LoadCsv(userList, userFilePath);
            
            string username = txtUsername.Text;
            string password = txtPassword.Text;
            bool rememberMe = chkrememberMe.Checked;
            if (userList.Count != 0)
            {
                for (int i = 0; i < userList.Count; i++)
                {
                    User user = userList[i];


                    if (user.IsValid(username, password))
                    {
                        //  user.RememberMe = rememberMe;
                        //  LoginedUser.getInstance().User = user;
                        label1.Text = "Success";
                        label1.ForeColor = Color.Green;

                        LoginDelay.Start();
                        //  Util.SaveCsv(userList, userFilePath);
                        return;
                    }

                    else
                    {
                       
                    }
                }
            }
            else
            {
                Register register = new Register();
                register.Show();
            }
           
              
                    
                   
            
        }

        private void LoginDelay_Tick(object sender, EventArgs e)
        {
            Login();
        }
        private void Login()
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Hide();
            LoginDelay.Stop();
        }
        private void checkRememberedUser()
        {
            foreach(User user in userList)
            {
                if(user.RememberMe)
                {
                    LoginedUser.getInstance().User = user;
                    LoginDelay.Interval = 10;
                    LoginDelay.Start();
                    return;
                }
            }
        }

        public  void btnRegister_Click(object sender, EventArgs e)
        {
             Register register = new Register();
            register.Show();
        }
    }
}
