﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonalApplication
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Usermanagement_Click(object sender, EventArgs e)
        {
            UserManagement userManagement1 = new UserManagement();
            userManagement1.Show();
            this.Hide();
        }

        private void btnPhoneBook_Click(object sender, EventArgs e)
        {
            PhoneBook phonebook = new PhoneBook();
            phonebook.Show();
            this.Hide();
        }

        private void btnReminder_Click(object sender, EventArgs e)
        {
            Reminder rm = new Reminder();
            rm.Show();
            this.Hide();
        }
    }
}
