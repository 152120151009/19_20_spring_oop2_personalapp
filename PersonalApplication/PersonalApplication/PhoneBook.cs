﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonalApplication
{
    public partial class PhoneBook : Form
    {
        public PhoneBook()
        {
            InitializeComponent();
          
        }
      
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void PhoneBooks()
        {
            string ayır = ",";
            string tableName = "Phone Book";
            string fileName = (@"data/phonebook.csv");

            DataSet dataset = new DataSet();
            StreamReader sr = new StreamReader(fileName);

            dataset.Tables.Add(tableName);
            dataset.Tables[tableName].Columns.Add("Name");
            dataset.Tables[tableName].Columns.Add("Surname");
            dataset.Tables[tableName].Columns.Add("Phone Number");
            dataset.Tables[tableName].Columns.Add("Address");
            dataset.Tables[tableName].Columns.Add("E-mail");
            dataset.Tables[tableName].Columns.Add("Description");
            dataset.Tables[tableName].Columns.Add("?");


            string allData = sr.ReadToEnd();
            string[] rows = allData.Split("\r".ToCharArray());

            foreach (string r in rows)
            {
                string[] items = r.Split(ayır.ToCharArray());
                dataset.Tables[tableName].Rows.Add(items);
            }
            this.dataGridView1.DataSource = dataset.Tables[0].DefaultView;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string path = @"data/phonebook.csv";
            string createPB = txtName.Text + "," + txtSurname.Text + "," + txtPhoneNumber.Text + "," +txtAddress.Text+","+txtAddress.Text +","+txtDescription.Text+ Environment.NewLine;
            File.WriteAllText(path, createPB);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            PhoneBooks();
        }
    }
}
