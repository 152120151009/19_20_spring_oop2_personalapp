﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonalApplication
{
    public partial class ReminderMessage : Form
    {
        public ReminderMessage()
        {
            InitializeComponent();
        }
        public string message = " ";
        private void ReminderMessage_Load(object sender, EventArgs e)
        {
            label1.Text = message;
        }
    }
}
