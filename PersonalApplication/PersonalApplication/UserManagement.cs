﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PersonalApplication
{
    public partial class UserManagement : Form
    {
        public UserManagement()
        {
            InitializeComponent();
        }

        private void UserManagement_Load(object sender, EventArgs e)
        {

        }
        private void UManagement()
        {
            string ayır = ",";
            string tableName = "User Management";
            string fileName = (@"data/user.csv");

            DataSet dataset = new DataSet();
            StreamReader sr = new StreamReader(fileName);

            dataset.Tables.Add(tableName);
            dataset.Tables[tableName].Columns.Add("Username");
            dataset.Tables[tableName].Columns.Add("Password");
            dataset.Tables[tableName].Columns.Add("Remember Me");
            dataset.Tables[tableName].Columns.Add("UserType");

            string allData = sr.ReadToEnd();
            string[] rows = allData.Split("\r".ToCharArray());

            foreach (string r in rows)
            {
                string[] items = r.Split(ayır.ToCharArray());
                dataset.Tables[tableName].Rows.Add(items);
            }
            this.dataGridView1.DataSource = dataset.Tables[0].DefaultView;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex>=0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                Usernametxt.Text = row.Cells["Username"].Value.ToString();
                UserTypetxt.Text = row.Cells["Usertype"].Value.ToString();

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UManagement();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            User UpdateUser = new User(Usernametxt.Text, Util.ComputeSha256Hash(Passwordtxt.Text), true, "user");
            string path = @"data/user.csv";
            string Updateuser = Usernametxt.Text + "," + Util.ComputeSha256Hash(Passwordtxt.Text) + "," + "0" + "user";
            File.AppendAllText(path, Updateuser, Encoding.UTF8);

        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Usernametxt.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            Passwordtxt.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            UserTypetxt.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();

        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            
     

        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Hide();
        }
    }
}
