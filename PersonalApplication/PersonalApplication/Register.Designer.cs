﻿namespace PersonalApplication
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRegister = new System.Windows.Forms.Button();
            this.lblrusername = new System.Windows.Forms.Label();
            this.llblrpassword = new System.Windows.Forms.Label();
            this.txtrusername = new System.Windows.Forms.TextBox();
            this.txtrpassword = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(332, 206);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(75, 23);
            this.btnRegister.TabIndex = 0;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // lblrusername
            // 
            this.lblrusername.AutoSize = true;
            this.lblrusername.Location = new System.Drawing.Point(261, 97);
            this.lblrusername.Name = "lblrusername";
            this.lblrusername.Size = new System.Drawing.Size(53, 13);
            this.lblrusername.TabIndex = 1;
            this.lblrusername.Text = "username";
            // 
            // llblrpassword
            // 
            this.llblrpassword.AutoSize = true;
            this.llblrpassword.Location = new System.Drawing.Point(262, 154);
            this.llblrpassword.Name = "llblrpassword";
            this.llblrpassword.Size = new System.Drawing.Size(52, 13);
            this.llblrpassword.TabIndex = 2;
            this.llblrpassword.Text = "password";
            // 
            // txtrusername
            // 
            this.txtrusername.Location = new System.Drawing.Point(332, 97);
            this.txtrusername.Name = "txtrusername";
            this.txtrusername.Size = new System.Drawing.Size(100, 20);
            this.txtrusername.TabIndex = 3;
            // 
            // txtrpassword
            // 
            this.txtrpassword.Location = new System.Drawing.Point(332, 151);
            this.txtrpassword.Name = "txtrpassword";
            this.txtrpassword.PasswordChar = '*';
            this.txtrpassword.Size = new System.Drawing.Size(100, 20);
            this.txtrpassword.TabIndex = 4;
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtrpassword);
            this.Controls.Add(this.txtrusername);
            this.Controls.Add(this.llblrpassword);
            this.Controls.Add(this.lblrusername);
            this.Controls.Add(this.btnRegister);
            this.Name = "Register";
            this.Text = "Register";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.Label lblrusername;
        private System.Windows.Forms.Label llblrpassword;
        private System.Windows.Forms.TextBox txtrusername;
        private System.Windows.Forms.TextBox txtrpassword;
    }
}