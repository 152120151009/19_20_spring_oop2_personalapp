﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
namespace PersonalApplication
{
    public partial class Reminder : Form
    {
        public Reminder()
        {
            InitializeComponent();
        }
        int a = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = DateTime.Now.ToString();
            btnAdd.Enabled = true;
            if (dateTimePicker1.Value <= DateTime.Now)
            {
                timer1.Stop();
                SoundPlayer player = new SoundPlayer();
                string soundpath = Application.StartupPath + "//alarm.wav";
                player.SoundLocation = soundpath;
                player.Play();
                a = 1;
            }
            btnStop.Enabled = true;
            if (a==1)
            {
                ReminderMessage Rm = new ReminderMessage();
                Rm.StartPosition = FormStartPosition.Manual;
                Rm.Location = new Point(Screen.PrimaryScreen.Bounds.Width - this.Width, Screen.PrimaryScreen.Bounds.Height - this.Height);
                Rm.message = message;
                Rm.ShowDialog();

            }
        }
        public string message = " ";
        private void Reminder_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            timer1.Start();
            btnStop.Enabled = false;
            message = textBox1.Text;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            btnAdd.Enabled = true;
            btnStop.Enabled = false;
            timer1.Enabled = false;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            MainWindow mw = new MainWindow();
            mw.Show();
            this.Hide();
        }
    }
}
