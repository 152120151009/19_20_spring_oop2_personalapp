﻿namespace PersonalApplication
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Usermanagement = new System.Windows.Forms.Button();
            this.btnSC = new System.Windows.Forms.Button();
            this.btnPhoneBook = new System.Windows.Forms.Button();
            this.btnReminder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Usermanagement
            // 
            this.Usermanagement.Location = new System.Drawing.Point(12, 12);
            this.Usermanagement.Name = "Usermanagement";
            this.Usermanagement.Size = new System.Drawing.Size(136, 30);
            this.Usermanagement.TabIndex = 0;
            this.Usermanagement.Text = "User Management";
            this.Usermanagement.UseVisualStyleBackColor = true;
            this.Usermanagement.Click += new System.EventHandler(this.Usermanagement_Click);
            // 
            // btnSC
            // 
            this.btnSC.Location = new System.Drawing.Point(12, 59);
            this.btnSC.Name = "btnSC";
            this.btnSC.Size = new System.Drawing.Size(136, 30);
            this.btnSC.TabIndex = 1;
            this.btnSC.Text = "Salary Calculater";
            this.btnSC.UseVisualStyleBackColor = true;
            // 
            // btnPhoneBook
            // 
            this.btnPhoneBook.Location = new System.Drawing.Point(12, 108);
            this.btnPhoneBook.Name = "btnPhoneBook";
            this.btnPhoneBook.Size = new System.Drawing.Size(136, 30);
            this.btnPhoneBook.TabIndex = 2;
            this.btnPhoneBook.Text = "Phone Book";
            this.btnPhoneBook.UseVisualStyleBackColor = true;
            this.btnPhoneBook.Click += new System.EventHandler(this.btnPhoneBook_Click);
            // 
            // btnReminder
            // 
            this.btnReminder.Location = new System.Drawing.Point(12, 157);
            this.btnReminder.Name = "btnReminder";
            this.btnReminder.Size = new System.Drawing.Size(136, 30);
            this.btnReminder.TabIndex = 3;
            this.btnReminder.Text = "Reminder";
            this.btnReminder.UseVisualStyleBackColor = true;
            this.btnReminder.Click += new System.EventHandler(this.btnReminder_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 286);
            this.Controls.Add(this.btnReminder);
            this.Controls.Add(this.btnPhoneBook);
            this.Controls.Add(this.btnSC);
            this.Controls.Add(this.Usermanagement);
            this.Name = "MainWindow";
            this.Text = "MainWindow";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Usermanagement;
        private System.Windows.Forms.Button btnSC;
        private System.Windows.Forms.Button btnPhoneBook;
        private System.Windows.Forms.Button btnReminder;
    }
}