﻿using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace PersonalApplication
{
    public static class Util
    {
        public static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }


        public static void LoadCsv(List<User> userlist, string csvPath)
        {
            using (var reader = new StreamReader(csvPath))
            {
               ///if(userlist.Count!=0)
               // {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        var values = line.Split(',');
                        string username = values[0];
                        string password = values[1];
                        bool rememberMe = values[2].Equals("1") ? true : false;
                        string usertypes = "Admin";
                        userlist.Add(new User(username, password, rememberMe,usertypes));
                    }
             //   }
              //  else
             //   {
                    
//}
               
            }
        }
        public static void SaveCsv(List<User> userlist, string csvPath)
        {
            using (var writer = new StreamWriter(csvPath))
            {
                foreach (User user in userlist)
                {
                    writer.WriteLine(user.ToString());
                }
            }

        }
        public static void Register(List<User> userlist, string csvPath)
        {
        }


    }
}
