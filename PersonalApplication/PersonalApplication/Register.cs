﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace PersonalApplication
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
           
            // public const string userFilePath = @"data/user.csv";
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            User createUser = new User(txtrusername.Text,Util.ComputeSha256Hash(txtrpassword.Text),true,"user");
            string path = @"data/user.csv";
            string createText = txtrusername.Text +","+ Util.ComputeSha256Hash(txtrpassword.Text)+","+"0"+"user" +Environment.NewLine;
            File.AppendAllText(path, createText, Encoding.UTF8);
           // Util.LoadCsv(userList, userFilePath);
            Form.ActiveForm.Close();

        }
    }
}
